﻿/*
 * It slices! It dices! 
 * Dicer - The Dice Rolling Simulator
 * <3 2021 Fuzzy Bellows LLC
 * I'll figure out the license crap later
 * for now, anyone can use this for whatever
 */

using System;

namespace Dicer
{
    class Program
    {
        static void Main(string[] args)
        {
            new Simulator().RunSims();
        }
    }
}