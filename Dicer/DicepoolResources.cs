using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Dicer
{
    enum DieState
    {
        Unrolled,
        Success,
        Failure
    }

    enum GraceEffect
    {
        TargetNumberOneLess,
        ExplodingDice,
        RerollFailed
    }

    enum DicepoolOutcome
    {
        CriticalSuccess,
        Success,
        Failure,
        CriticalFailure
    }

    struct Die
    {
        public DieState dieState;
        public int dieVal;
        public override string ToString()
        {
            return dieVal.ToString();
        }
    }

    class Dicepool
    {
        static private Random r = new Random();
        static private Func<int> getDieResult = delegate () { return r.Next(Simulation.sides) + 1; };//c#'s random starts at 0, +1 makes it act like a die
        static private Func<Die, DieState> evaluateDieResult = (x) => { if (x.dieVal < Simulation.valToBeat) { return DieState.Failure; } else { return DieState.Success; } }; //tweak for critical failure chance; critical success chance.
        static private Func<Die[], int> findSuccesses = (x) => x.Count(z => z.dieState == DieState.Success);
        static private Func<Die[], int> findFailures = (x) => x.Count(z => z.dieState == DieState.Failure);
        static private Func<Die[], int> findTens = (x) => x.Count(z => z.dieVal == 10);
        static private Func<Die[], int> findOnes = (x) => x.Count(z => z.dieVal == 1);

        static private Die[] baseDicepool = null;
        static private Func<Die[], Die[]> targetNumberOneLess = (x) => x.Select(y => {if (y.dieVal >= (Simulation.valToBeat - 1)) { y.dieState = DieState.Success; } else { y.dieState = DieState.Failure; } return y; }).ToArray<Die>();
        static private Func<Die[], Die[]> getExplodedDice = (x) => { Die[] newArray = new Die[x.Length + findTens(x)]; int newDiceNeeded = findTens(x); Array.Copy(x, newArray, x.Length); for (int i = (newArray.Length - newDiceNeeded); i < newArray.Length; i++) { newArray[i] = new Die() { dieState = DieState.Unrolled, dieVal = -1 }; }; return newArray; };
        static private Func<Die[], Die[]> resetFailedDice = (x) => x.Where(z => z.dieState == DieState.Success).ToArray<Die>().Concat(x.Where(z => z.dieState == DieState.Failure).Take(3).ToArray<Die>().Select(a => { a.dieState = DieState.Unrolled; a.dieVal = -1; return a; }).ToArray<Die>()).ToArray<Die>().Concat(x.Where(z => z.dieState == DieState.Failure).Skip(3)).ToArray<Die>();
        private int size;
        private Die[] dice;
        private int tally;                                          //the number of successes needed for the action to succeed
        private int negativeTally;                                  //"negative" tally -- important for certain calculations
        private List<Func<Die[], Die[]>> activeGraceEffects;        //the "Grace Effects" which change how the dicepool behaves
        public List<Func<Die[], Die[]>> ActiveGraceEffects
        {
            get { return activeGraceEffects; }
            set { activeGraceEffects = value; }
        }

        private int successes;                                      //number of successes
        private int failures;                                       //number of failures
        private int tenCount;                                       //number of 10's -- important for critical successes
        private int oneCount;                                       //number of 1's -- important for critical failures
        private const int criticalSuccessOffset = 1;
        private const int criticalFailureOffset = 6;
        private int criticalSuccessTenTarget;
        private int criticalFailureOneTarget;
        public enum Graces
        {
            Blessed,        // Target - 1
            Multiplying,    // Exploding 10's
            Unfailing       // Reroll failed
        }   

        //initialize the basedicepool in particular
        static private void InitializeBaseDicepool(int size)
        {
            Dicepool.baseDicepool = new Die[size];
            baseDicepool = (baseDicepool.Select(x => new Die() { dieState = DieState.Unrolled, dieVal = -1 })).ToArray<Die>();     //DieState.Unrolled should be obvious; -1 is just a "blank" value for a die.
        }

        //return a copy of baseDicePool on command
        static private Die[] CopyBaseDicePool()
        {
            return (Die[])Dicepool.baseDicepool.Clone();
        }

        //reset the dicepool to a "clean" state
        public void ResetDicePool()
        {
            this.dice = Dicepool.CopyBaseDicePool();
        }

        //initialize the dice to an unrolled state
        public void DicepoolHelper(int size)
        {
            if (Dicepool.baseDicepool == null)
            {
                Dicepool.InitializeBaseDicepool(size);
            }
            ResetDicePool();            
            negativeTally = -(tally);
            criticalSuccessTenTarget = tally + criticalSuccessOffset;
            criticalFailureOneTarget = negativeTally + criticalFailureOffset;
        }

        //assuming a dicepool of 10, a Tally of 1, and no Grace Effects unless otherwise specified
        public Dicepool(int size = 10, int tally = 1)
        {
            this.size = size;
            this.activeGraceEffects = new List<Func<Die[], Die[]>>();
            this.DicepoolHelper(size);
            this.tally = tally;
        }

        //add a grace effect
        public void addGraceEffect(Graces grace)
        {
            switch(grace) {
                case Graces.Blessed:
                    activeGraceEffects.Add(targetNumberOneLess);
                    bool rollOnce = false;
                    break;
                case Graces.Multiplying:
                    activeGraceEffects.Add(getExplodedDice);
                    break;
                case Graces.Unfailing:
                    activeGraceEffects.Add(resetFailedDice);
                    break;
            }
        }

        //roll our dicepool
        public Tuple<DicepoolOutcome, double> roll()
        {
            bool rollOnce = false;
            int priorSize = size;
            int newSize = 0;
            int skipSize = 0;
            int takeSize = 0;
            Die[] intermediateDice;
            try
            {
                if (activeGraceEffects.Count == 0) {
                    throw new InvalidOperationException("Can't perform Grace effects with none defined.");
                }
                foreach (Func<Die[], Die[]> graceEffect in activeGraceEffects)
                //which grace would be optimal to apply first? you're going to get different results in that light.
                //there's a lot to play with in terms of how they're applied, when they're applied, what order they're applied in, etc.
                {
                    do
                    {
                        newSize = dice.Length;
                        if (rollOnce == false)
                        {
                            skipSize = 0;
                            takeSize = size;
                            rollOnce = true;
                        }
                        else
                        {
                            skipSize = priorSize;
                            takeSize = newSize - priorSize;
                        }                        
                        intermediateDice = dice.Skip(skipSize).Take(takeSize).Select(x => { if (x.dieVal == -1) { x.dieVal = getDieResult(); } return x; }).ToArray<Die>();                        
                        dice = dice.Select(x => { if (x.dieVal == -1) { x.dieVal = getDieResult(); } return x; }).ToArray<Die>();
                        // foreach (Die die in intermediateDice) {
                        //     Console.Write(die + " ");
                        // }
                        // Console.WriteLine();
                        intermediateDice = (intermediateDice.Select(x => { x.dieState = evaluateDieResult(x); return x; })).ToArray<Die>();
                        dice = (dice.Select(x => { x.dieState = evaluateDieResult(x); return x; })).ToArray<Die>();
                        if (dice.Count(z => z.dieVal == -1) == size)
                        {
                            dice = intermediateDice;                            
                            priorSize = dice.Length;
                            dice = graceEffect(dice);
                        }
                        else
                        {
                            priorSize = dice.Length;
                            // foreach (Die die in intermediateDice)
                            // {
                            //     Console.Write(die + " ");
                            // }
                            // Console.WriteLine();                            
                            dice = dice.Take(skipSize).Concat(graceEffect(intermediateDice)).ToArray<Die>();
                        }                                               
                        // foreach (Die die in dice) {
                        //     Console.Write(die + " ");
                        // }
                        // Console.WriteLine();
                    } while (dice.Count(x => x.dieState == DieState.Unrolled) > 0);
                }
            }
            catch
            {             
                dice = (dice.Where(x => x.dieVal == -1).Select(x => { x.dieVal = getDieResult(); return x; })).ToArray<Die>();
                dice = (dice.Select(x => { x.dieState = evaluateDieResult(x); return x; })).ToArray<Die>();
            }
            //make this a tuple that returns from a compound function
            successes = findSuccesses(dice);
            failures = findFailures(dice);
            tenCount = findTens(dice);
            oneCount = findOnes(dice);
            Tuple<DicepoolOutcome> dicepoolOutcomeTuple = null;
            Tuple<double> totalSegmentTuple = new Tuple<double>(Convert.ToDouble(dice.Select(x => x.dieVal).Sum()));
            if (successes == 0 && oneCount >= criticalFailureOneTarget)
            {
                dicepoolOutcomeTuple = new Tuple<DicepoolOutcome>(DicepoolOutcome.CriticalFailure);
            }
            else if (successes < tally)
            {
                dicepoolOutcomeTuple = new Tuple<DicepoolOutcome>(DicepoolOutcome.Failure);
            }
            else if (tenCount >= criticalSuccessTenTarget)
            {
                dicepoolOutcomeTuple = new Tuple<DicepoolOutcome>(DicepoolOutcome.CriticalSuccess);
            }
            else
            {
                dicepoolOutcomeTuple = new Tuple<DicepoolOutcome>(DicepoolOutcome.Success);
            }
            return new Tuple<DicepoolOutcome, double>(dicepoolOutcomeTuple.Item1, totalSegmentTuple.Item1);
        }
    }
}