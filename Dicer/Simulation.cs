﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace Dicer
{
    class Simulation
    {
        protected double total = 0;
        protected int successes = 0;
        protected int failures = 0;
        protected double average = -1;
        protected int actualNumOfRolls = 0;
        protected int totalNumOfRolls;
        protected string name;
        public int Embers = 1;
        static public int valToBeat;
        static public int sides;
        protected int tally;
        private bool blessedGrace;
        private bool multiplyingGrace;
        private bool unfailingGrace;

        //set up the object with overrides
        public Simulation(string name = "Default Roll Simulation", int totalNumOfRolls = 20000, int tally = 1, int sides = 10, int valToBeat = 7, bool blessedGrace = false, bool multiplyingGrace = false, bool unfailingGrace = false)
        {
            this.name = name;
            this.totalNumOfRolls = totalNumOfRolls;
            this.tally = tally;
            Simulation.sides = sides;
            Simulation.valToBeat = valToBeat;
            this.blessedGrace = blessedGrace;
            this.multiplyingGrace = multiplyingGrace;
            this.unfailingGrace = unfailingGrace;
        }

        public virtual void RunSimulation()
        {
          Action<Tuple<DicepoolOutcome,double>> unwrapDicepoolReturns = (x) => { if (x.Item1 == DicepoolOutcome.Success || x.Item1 == DicepoolOutcome.CriticalSuccess) { this.successes++; } else { this.failures ++; } this.total += x.Item2; };
          Dicepool dicepool = new Dicepool(tally: this.tally);
          if (blessedGrace) {
            dicepool.addGraceEffect(Dicepool.Graces.Blessed);
          }
          if (multiplyingGrace) {
            dicepool.addGraceEffect(Dicepool.Graces.Multiplying);
          }
          if (unfailingGrace) {
            dicepool.addGraceEffect(Dicepool.Graces.Unfailing);
          }
          for (int i = 0; i < totalNumOfRolls; i++)
          {
            actualNumOfRolls++;
            unwrapDicepoolReturns(dicepool.roll());
            dicepool.ResetDicePool();
          }
          average = total / Convert.ToDouble(totalNumOfRolls);
        }

        public void PrintResults()
        {
            Console.WriteLine("Completed: " + name);
            Console.WriteLine("Ran " + totalNumOfRolls.ToString() + " simulations of " + sides.ToString() + "-sided dice...");
            Console.WriteLine("Actual total # of rolls: " + actualNumOfRolls);
            Console.WriteLine("Average: " + average.ToString());
            Console.WriteLine("Value to Beat: " + valToBeat.ToString());
            Console.WriteLine("Tally: " + tally);
            Console.WriteLine("Successes: " + successes.ToString());
            Console.WriteLine("Failures: " + failures.ToString());
            Console.WriteLine();
        }
    }
}