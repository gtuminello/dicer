﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Dicer
{
    class Simulator
    {
        public Simulator()
        {
            Console.WriteLine("Dicer launched! Time for some simulations...");
            Console.WriteLine();
        }

        //maybe this can accept args eventually
        public void RunSims()
        {
            // Base parameters for the simulations.
            int totalNumOfRolls = 50000;
            int tally = 2;

            //Run simulations.
            Simulation defaultSim = new Simulation(tally: tally, totalNumOfRolls: totalNumOfRolls);
            defaultSim.RunSimulation();
            defaultSim.PrintResults();
            Simulation blessedSim = new Simulation(tally: tally, totalNumOfRolls: totalNumOfRolls, name: "Blessed Grace Simulation", blessedGrace: true);
            blessedSim.RunSimulation();
            blessedSim.PrintResults();
            Simulation multiplyingSim = new Simulation(tally: tally, totalNumOfRolls: totalNumOfRolls, name: "Multiplying Grace Simulation", multiplyingGrace: true);
            multiplyingSim.RunSimulation();
            multiplyingSim.PrintResults();
            Simulation unfailingSim = new Simulation(tally: tally, totalNumOfRolls: totalNumOfRolls, name: "Unfailing Grace Simulation", unfailingGrace: true);
            unfailingSim.RunSimulation();
            unfailingSim.PrintResults();
        }
    }
}
